<?php

class UserRole extends Eloquent {
	
	protected $table = 'userroles';

	public function users()
	{
		return $this->hasMany('User');
	}
}