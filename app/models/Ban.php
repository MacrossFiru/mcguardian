<?php


class Ban extends Eloquent {
	
	protected $table = 'bans';

	public function user()
	{
		return $this->belongsTo('User', 'submit_by');
	}

	public function comments()
	{
		return $this->hasMany('Comment');
	}

	public function bantype()
	{
		return $this->belongsTo('BanType', 'ban_type');
	}
}