<?php


class ActionType extends Eloquent {
	
	protected $table = 'actiontypes';

	public function comments()
	{
		return $this->hasMany('Comment');
	}
}