<?php

class Comment extends Eloquent {
	
	protected $table = 'comments';

	public function ban()
	{
		return $this->belongsTo('Ban');
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function ActionType()
	{
		return $this->belongsTo('ActionType');
	}
}