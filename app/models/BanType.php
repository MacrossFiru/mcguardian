<?php

class BanType extends Eloquent {
	
	protected $table = 'bantypes';

	public function bans()
	{
		return $this->hasMany('Ban');
	}
}