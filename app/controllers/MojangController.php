<?php

class MojangController extends BaseController {

	


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$client = new GuzzleHttp\Client();
		$mojangresponse = $client->get('https://api.mojang.com/users/profiles/minecraft/' . $id);
		$response = Response::json($mojangresponse->json(), $mojangresponse->getStatusCode());
		$response->header('Content-Type', 'application/json; charset=utf-8');
		return $response;
	}


}
