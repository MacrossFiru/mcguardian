<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function() 
{
    //$bans = BanController::index();
    $bans = Ban::with('bantype', 'user')->orderBy('applied_date', 'created_at')->get();
    return View::make('banlist')->with('bans', $bans);
});

Route::resource('mojang', 'MojangController', [ 'only' => [ 'show' ]]);


//Route::resource('ban', 'BanController');

Route::get('/butt', function()
{
    return Ban::with('bantype', 'user')->get();
});

