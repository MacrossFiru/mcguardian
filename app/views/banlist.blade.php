@extends('templates.main')
@section('content')

<h3>Minecraft Bans</h3><br>


<table class="table table-hover table-condensed">
    <thead>
        <tr>
            <th class='date'>Date</th>
            <th>Name</th>
            <th>MojangID</th>
            <th>Ban Type</th>
            <th>Submitted By</th>
            <th>Ban Active</th>
            <th  class='numeric'>IPv4 Address</th>
            <th>Actions</th>
        </tr>
    </thead>

 
    <tbody>
 
        @foreach($bans as $ban)
        <tr id={{ "banrow-" . $ban->id }} >
            <td class='date'>{{ $ban->applied_date }}</td>
            <td>{{ $ban->player_name }}</td>
            <td>{{ $ban->mojang_uuid }}</td>
            <td>{{ $ban->bantype->name }}</td>
            <td>{{ $ban->user->name }}</td>
            <td>@if($ban->open)
                    True
                @else
                    False
                @endif
            </td>
            <td>{{ $ban->ipv4address }}</td>
            <td>
                <button class="btn btn-xs btn-info glyphicon glyphicon-question-sign" id={{ "btninfo-" . $ban->id }}></button>
                <button class="btn btn-xs btn-primary glyphicon glyphicon-plus-sign" id={{ "btncomment-" . $ban->id }}></button>                
                <button class="btn btn-xs btn-danger glyphicon glyphicon-remove" id={{ "btndelete-" . $ban->id }}></button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>


@endsection