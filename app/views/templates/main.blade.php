<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" contents="IE=edge">
        <title>{bit:griefer}.MCGuardian</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <link href="{{ URL::to('css/mcguardian.min.css') }}" rel='stylesheet' type='text/css'>


    </head>

    <body>
        <div id="header_bg"></div>
        <div class="navbar navbar-inverse" id="mcb_navbar">
            <nav class="navbar-header" role="navigation">
                <a class="navbar-brand" href="#">McBans</a>
            </nav>
        </div>

        <div class="container-fluid">
            <div class="row-fluid" id="mainview-row">
                <div class="col-md-2 container" id="mcb_sidebar">
                    <ul class="list-group">
                        <p class="list-group-item" id="mcb_list-group-header">Main Menu</p>
                        <a href="#" class="list-group-item active">Home</a>
                        <a href="#" class="list-group-item disabled">New Bans</a>
                        <a href="#" class="list-group-item disabled">Temporary Bans</a>
                        <a href="#" class="list-group-item disabled">Permanent Bans</a>
                        <a href="#" class="list-group-item disabled">Search</a>
                        <a href="#" class="list-group-item">Login</a>
                    </ul>
                </div>



                <div class="col-md-10 jumbotron" id="mcb_jumbotron">
                    <div class="container" id="mcb_viewport">
                     @yield('content')



                    </div>
                </div>
                <!--yield('pagination') -->
            </div>
        </div>

        <script src="{{ URL::to('js/mcguardian.min.js') }}"></script>
    </body>
</html>
