<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bans', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->date('applied_date');
			$table->date('reminder');
			$table->string('player_name', 50);
			$table->string('mojang_uuid', 45)->default('00000000-0000-0000-0000-000000000000');
			$table->integer('ban_type')->unsigned();
			$table->foreign('ban_type')->references('id')->on('bantypes');
			$table->integer('submit_by')->unsigned();
			$table->foreign('submit_by')->references('id')->on('users');
			$table->boolean('open');
			$table->string('ipv4address', 15)->default('000.000.000.000');
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bans');
	}

}
