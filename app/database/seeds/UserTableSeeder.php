<?php

class UserTableSeeder extends Seeder {
	
	public function run()
	{

        DB::table('users')->delete();
        User::create(
            [ 
                'username'=>'AdminUser', 
                'password'=>'userpassword', 
                'name'=>"Jane Citizen",
                'last_login'=>'2010-01-01',
                'role_id'=>'1'
            ]);

        User::create(
            [ 
                'username'=>'OperatorUser', 
                'password'=>'userpassword', 
                'name'=>"Tom Down",
                'last_login'=>'2010-01-01',
                'role_id'=>'2'
            ]);

        User::create(
            [ 
                'username'=>'guest', 
                'password'=>'disabled', 
                'name'=>"Guest",
                'last_login'=>'2010-01-01',
                'role_id'=>'3'
            ]);
	
	}

}