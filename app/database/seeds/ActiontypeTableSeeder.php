<?php

class ActiontypeTableSeeder extends Seeder {
    
    public function run()
    {
        DB::table('actiontypes')->delete();
        ActionType::create(
            [
                'name'=>'Create',
                'description'=>'Ban Entry is Submitted.'
            ]);
        ActionType::create(
            [
                'name'=>'Edit',
                'description'=>'Ban entry has been edited.'
            ]);
        ActionType::create(
            [
                'name'=>'Comment',
                'description'=>'Additional comment added.'
            ]);
        ActionType::create(    [
                'name'=>'Close',
                'description'=>'Ban record closed, ban reversed.'
            ]);
    }

}