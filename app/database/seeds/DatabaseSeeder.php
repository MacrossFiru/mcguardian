<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('BantypeTableSeeder');
		$this->call('ActiontypeTableSeeder');
		$this->call('UserRoleTableSeeder');
		$this->call('UserTableSeeder');
		$this->call('BanTableSeeder');


	}

}
