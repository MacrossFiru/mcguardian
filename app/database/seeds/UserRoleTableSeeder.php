<?php

class UserRoleTableSeeder extends Seeder {
	
	public function run()
	{
		DB::table('userroles')->delete();
		UserRole::create(
			[ 
				'name'=>'Administrator', 
				'description'=>'The Administrator has the highest level accesss.'
			]);
		UserRole::create(
			[
				'name'=>'Operator', 
				'description'=>'Operators are able to submit, comment and modify ban records'
			]);
		UserRole::create(
			[
				'name'=>'ReadOnly', 
				'description'=>'The read-only user may view bans but cannot edit them.' 
			]);

	}

}