<?php

class BanTableSeeder extends Seeder {
    
    public function run()
    {
        DB::table('bans')->delete();

        for ($i = 1; $i <= 7; $i++) {
            Ban::create([
                    'applied_date'=>'2012-10-10',
                    'reminder'=>'2012-10-30',
                    'player_name'=>'Player0' . $i,
                    'mojang_uuid'=>'00000000-0000-0000-' . $i . '000-000000000000',
                    'ban_type'=>$i,
                    'submit_by'=>'1',
                    'open'=>'true',
                    'ipv4address'=>'100.100.100.100'
                ]);            
        }

    }
}