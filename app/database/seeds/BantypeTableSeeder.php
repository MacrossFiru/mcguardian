<?php

class BantypeTableSeeder extends Seeder {
    
    public function run()
    {
        DB::table('bantypes')->delete();
        BanType::create(
                [
                    'id'=>'1',
                    'name'=>'Temporary',
                    'description'=>'Temporary Bans expire after a set period of time and are applied to users as a time-out.'   
                ]);

                BanType::create(
                [
                    'id'=>'2',
                    'name'=>'Permanent',
                    'description'=>'Permanent Bans are applied to users who have made significant infraction.'
                ]);
                BanType::create(
                [
                    'id'=>'3',
                    'name'=>'Mute',
                    'description'=>'Muting is a temporary solution to spammers.'
                ]);
                BanType::create(
                [               
                     'id'=>'4',  
                    'name'=>'Jail',
                    'description'=>'A user is locked into a time-out zone but can still interact with other users.'
                ]);
                BanType::create(
                [               
                     'id'=>'5',
                    'name'=>'Raised In Error',
                    'description'=>'If the ban record is raised by mistake, a moderator can mark it accordingly for a senior admin to delete.'
                ]);
                BanType::create(
                [
                    'id'=>'6',
                    'name'=>'Permission Change',
                    'description'=>'A change of permissions has been applied to this user.'
                ]);
                BanType::create(
                [
                    'id'=>'7',
                    'name'=>'Observation',
                    'description'=>'A moderator has requested observation of this users behavior.'
                ]);
    }

}