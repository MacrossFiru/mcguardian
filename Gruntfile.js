module.exports = function (grunt) {
    var buildTarget = grunt.option('target') || 'dev';

    grunt.initConfig({
        less: {
            main: {
                options: {
                    compress          : (buildTarget !== 'dev'),
                    sourceMap         : true,
                    sourceMapURL      : './mcguardian.min.css.map',
                    sourceMapRootpath : '../../'
                },
                files: { 'public/css/mcguardian.min.css': 'public_src/less/main.less' }
            }
        }, // eo less

        uglify: {
            options: {
                compress  : (buildTarget !== 'dev'),
                mangle    : (buildTarget !== 'dev'),
                sourceMap : true
            },

            main: {
                files: {
                    'public/js/mcguardian.min.js': [
                        'bower_components/jquery/dist/jquery.js',
                        'bower_components/bootstrap/js/bootstrap.js',
                        'public_src/javascript/common/*.js'
                    ]
                }
            },

            others: {
                files: [{
                    expand : true,
                    cwd    : 'public_src/javascript/',
                    src    : 'mini-builds/**/*.js',
                    dest   : 'public/js/',
                    ext    : '.min.js'
                }]
            }
        }, // eo uglify

        watch: {
            less: {
                files: [
                    'bower_components/bootstrap/bootstrap.css',
                    'public_src/less/**/*.less'
                ],
                tasks: ['less']
            },

            uglify: {
                files: [
                    'bower_components/jquery/dist/jquery.js',
                    'bower_components/bootstrap/js/bootstrap.js',
                    'public_src/javascript/**/*.js'
                ],
                tasks: ['uglify']
            }
        } // eo watch
    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['uglify', 'less']);
};
